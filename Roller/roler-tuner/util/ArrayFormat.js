module.exports = class ArrayFormat{
    /**
     *Creates an instance of ArrayFormat.
     * @param {*} x
     * @param {*} nBCollumn
     * @param {*} title
     */
    constructor(x,nBCollumn) {
        this.x = x;
        this.nBCollumn = nBCollumn;
        this.title=[];

    }
    /**
     *Permet l'ajout des titres a ajouter
     * @param {String[]} _title : Tableau de string des titres
     */
    setTitles(_title){
        try{
            for(var i=0; i<_title.length; i++){
                this.title[i] = _title[i];
            }
            console.log("Les titres ont bien été ajouté");
        }catch(error){
            console.error(toString(error));
        }

    }
    /**
     * Fonction permettant de voir les titres présents
     * @returns
     */
    getTitles(){
        return this.title;
    }
    /**
     *Affiche tout un tableau formatté
     *
     * @param {*} _data
     */
    formatConsole(_data){
        this.consoleLine();
        this.consoleHead();
        this.consoleLine();
        this.consoleContent(_data);
        this.consoleLine();
    }
    /**
     *Créer l'entête du tableau, rien a modifier dessus
     *
     */
    consoleHead(){
        var text = "|";
        var finalText = "";
        var long = this.x/this.nBCollumn;
        for(var i=1; i<=this.title.length;i++){
            text = text+" "+this.title[i-1];
            for(var j=text.length; j<long; j++){
                text = text + " ";
            }
            finalText = finalText+text; text = "|";
        }
        console.log(finalText+"|");
    }
    /**
     *Affiche le contenu du tableau

     *Attention ! C'est trés certainement cette partie qu'il faut modifier !
     * @param {} _data
     */
    consoleContent(_data){
        for(var l=0; l<_data.length;l++){
            var text = "|";
            var finalText = "";
            
            text = text + (l+1) + " ";
            text = this.consoleSpacer(text);
            finalText = finalText+text; text = "|";

            text = text+" "+_data[l].name;
            text = this.consoleSpacer(text);
            finalText = finalText+text; text = "|";

            text = text+" "+_data[l].spe.name;
            text = this.consoleSpacer(text);
            finalText = finalText+text; text = "|";

            console.log(finalText+"|");
        }
    }
    /**
     *Permet de faire les lignes du tableau
     *
     */
    consoleLine(){
        var text = "";
        for(var i=0; i<=this.x; i++){
            text=text+"-";
        }
        console.log(text);
    }

    /**
     * Fait les espaces aprés les chaines de caractéres
     * @param {String} text 
     */
    consoleSpacer(text){
        for(var j=text.length; j<this.x/this.nBCollumn; j++){
            text = text + " ";
        }
        return text;
    }
}
