const Game = require("./business/Game.js");
const Spe = require("./business/Spe.js");
const Player = require("./business/Player.js");
const ArrayFormat = require("./util/ArrayFormat.js");

var testGame = new Game("test1");

for(var i = 0; i<10; i++){
    var spe = new Spe("test_spe"+i);
    var perso = new Player("test_player"+i, spe);
    testGame.addPlayer(perso);
}

var arrayFormat = new ArrayFormat(60,3);
arrayFormat.setTitles(["Numéro","Nom","Spécialité"]);
arrayFormat.formatConsole(testGame.getPlayers());