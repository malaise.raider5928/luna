//Code générer pour le projet {luna}

//----------------Import----------------//
require('dotenv').config();
const Interceptor = require('./util/Interceptor');
const Discord = require('discord.js');
//--------------------------------------//

//---------------Paramétres-------------//
//--------------------------------------//

//--------------Constructeur------------//
const client = new Discord.Client();
const msg = new Interceptor(client);
//--------------------------------------//

//-------------Getter et setter---------//
//--------------------------------------//

//---------------Méthodes---------------//
client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});


client.login(process.env.DISCORD_TOKEN).then(r => console.log(r));
//--------------------------------------//



