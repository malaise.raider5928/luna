//Code générer pour le projet {luna}

//----------------Import----------------//
const Discord = require('discord.js');
const fs = require('fs');
const answer_json = require('./answer.json');
//--------------------------------------//
module.exports = class Message {
//---------------Paramétres-------------//
//--------------------------------------//

//--------------Constructeur------------//
    constructor(client) {
        this.client = client;
        client.on('message', msg => {
            this.sendMessages(msg);
        });
    }

//--------------------------------------//

//-------------Getter et setter---------//
//--------------------------------------//

//---------------Méthodes---------------//
    sendMessages(msg) {
        const message = (msg.content).split(":")[1];
        if(msg.author.id !== this.client.user.id){
            if(answer_json[message] !== {}){
                const data = answer_json[message];
                if(data.type === "reply"){
                    msg.reply(data.text);
                }
                else if(data.type === "send"){
                    msg.channel.send(data.text);
               }
            }
        }
    }

//--------------------------------------//

};
